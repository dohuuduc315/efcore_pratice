﻿using Movie_Console_App_Advance.IRepopsitory;
using Movie_Console_App_Advance.Repository;

namespace Movie_Console_App_Advance
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _dbContext;
        private readonly IMovieRepository _movieRepository;
        private readonly IReviewRepository _reviewRepository;

        public UnitOfWork(AppDbContext context)
        {
            _dbContext = context;
            _movieRepository = new MovieRepository(_dbContext);
            _reviewRepository = new ReviewRepository(_dbContext);
        }

        public IMovieRepository MovieRepository => _movieRepository;
        public IReviewRepository ReviewRepository => _reviewRepository;

        public void Dispose()
        {
            _dbContext.Dispose();
        }

        public async Task<int> SaveChangeAsync() => await _dbContext.SaveChangesAsync();
    }
}
