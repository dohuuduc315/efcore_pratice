﻿using Microsoft.EntityFrameworkCore;

namespace Movie_Console_App_Advance
{
    public class AppDbContext : DbContext
    {
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Review> Reviews { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connect = "Data Source=DESKTOP-Q5ODVD3\\SQLEXPRESS;user=sa;password=12345;Database=Movie_Cosole_App;Trusted_Connection=True;TrustServerCertificate=True";
            optionsBuilder.UseSqlServer(connect);
        }
    }
}
