﻿namespace Movie_Console_App_Advance.IService
{
    public interface IReviewService
    {
        Task AddReview();
        Task UpdateReview(int id);
        Task DeleteReview(int id);
    }
}
