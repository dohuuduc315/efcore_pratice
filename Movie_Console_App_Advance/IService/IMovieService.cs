﻿namespace Movie_Console_App_Advance.IService
{
    public interface IMovieService
    {
        Task AddMovie();
        Task UpdateMovie(int id);
        Task DeleteMovie(int id);
        Task<List<Movie>> ViewAllMovie();
        Task ViewMovieReview(int id);
    }
}
