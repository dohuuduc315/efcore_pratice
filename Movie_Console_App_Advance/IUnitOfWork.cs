﻿using Movie_Console_App_Advance.IRepopsitory;

namespace Movie_Console_App_Advance
{
    public interface IUnitOfWork : IDisposable
    {
        public IMovieRepository MovieRepository { get; }
        public IReviewRepository ReviewRepository { get; }
        public Task<int> SaveChangeAsync();
    }
}
