﻿using Movie_Console_App_Advance;
using Movie_Console_App_Advance.Service;

var unitOfWork = new UnitOfWork(new AppDbContext());
var movieService = new MovieService(unitOfWork);
var reviewService = new ReviewService(unitOfWork);
int choice = 0;
do
{
    Console.WriteLine("-------------MENU------------");
    Console.WriteLine("1. View");
    Console.WriteLine("2. Add");
    Console.WriteLine("3. Update");
    Console.WriteLine("4. Delete");
    Console.WriteLine("5. Exit");
    Console.Write("Enter your choice (1-5): ");
    if (!int.TryParse(Console.ReadLine(), out choice))
    {
        Console.WriteLine("Invalid choice");
    }
    switch (choice)
    {
        case 1:
            do
            {
                Console.WriteLine("-------------VIEW------------");
                Console.WriteLine("1. View all movies");
                Console.WriteLine("2. View a specific movie and its reviews");
                Console.WriteLine("3. Back to menu");
                Console.Write("Enter your choice (1-3): ");
                if (!int.TryParse(Console.ReadLine(), out choice))
                {
                    Console.WriteLine("Invalid choice");
                }
                switch (choice)
                {
                    case 1:
                        var listMovie = await movieService.ViewAllMovie();
                        if (listMovie is not null)
                        {
                            foreach (var movie in listMovie)
                            {
                                Console.WriteLine($"{movie.Id}\t{movie.Title}\t{movie.ReleaseDate:d}\t{movie.Director}\t{movie.Description}\t{movie.ImageUrl}\t{movie.Rating:F1}");
                            }
                        }
                        break;
                    case 2:
                        Console.Write("Input MovieId: ");
                        if (!int.TryParse(Console.ReadLine(), out int movieId))
                        {
                            Console.WriteLine("Invalid Id");
                        }
                        await movieService.ViewMovieReview(movieId);
                        break;
                    default:
                        break;
                }
            } while (choice > 0 && choice < 3);
            break;
        case 2:
            Console.WriteLine("-------------ADD------------");
            Console.WriteLine("1. Add a new movie");
            Console.WriteLine("2. Add a new review for a movie");
            Console.WriteLine("3. Back to menu");
            Console.Write("Enter your choice (1-3): ");
            if (!int.TryParse(Console.ReadLine(), out choice))
            {
                Console.WriteLine("Invalid choice");
            }
            switch (choice)
            {
                case 1:
                    await movieService.AddMovie();
                    break;
                case 2:
                    await reviewService.AddReview();
                    break;
                default:
                    break;
            }
            break;
        case 3:
            Console.WriteLine("-------------UPDATE------------");
            Console.WriteLine("1. Edit an existing movie");
            Console.WriteLine("2. Edit an existing review for a movie");
            Console.WriteLine("3. Back to menu");
            Console.Write("Enter your choice (1-3): ");
            if (!int.TryParse(Console.ReadLine(), out choice))
            {
                Console.WriteLine("Invalid choice");
            }
            switch (choice)
            {
                case 1:
                    Console.Write("Input MovieId to update: ");
                    if (!int.TryParse(Console.ReadLine(), out int movieId))
                    {
                        Console.WriteLine("Invalid Id");
                    }
                    await movieService.UpdateMovie(movieId);
                    break;
                case 2:
                    Console.Write("Input ReviewId to update: ");
                    if (!int.TryParse(Console.ReadLine(), out int reviewId))
                    {
                        Console.WriteLine("Invalid Id");
                    }
                    await reviewService.UpdateReview(reviewId);
                    break;
                default:
                    break;
            }
            break;
        case 4:
            Console.WriteLine("-------------DELETE------------");
            Console.WriteLine("1. Delete a movie");
            Console.WriteLine("2. Delete a review for a movie");
            Console.WriteLine("3. Back to menu");
            Console.Write("Enter your choice (1-3): ");
            if (!int.TryParse(Console.ReadLine(), out choice))
            {
                Console.WriteLine("Invalid choice");
            }
            switch (choice)
            {
                case 1:
                    Console.Write("Input MovieId to delete: ");
                    if (!int.TryParse(Console.ReadLine(), out int movieId))
                    {
                        Console.WriteLine("Invalid Id");
                    }
                    await movieService.DeleteMovie(movieId);
                    break;
                case 2:
                    Console.Write("Input ReviewId to delete: ");
                    if (!int.TryParse(Console.ReadLine(), out int reviewId))
                    {
                        Console.WriteLine("Invalid Id");
                    }
                    await reviewService.DeleteReview(reviewId);
                    break;
                default:
                    break;
            }
            break;
        default:
            break;
    }
} while (choice > 0 && choice < 5);