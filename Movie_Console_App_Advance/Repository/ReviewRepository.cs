﻿using Movie_Console_App_Advance.IRepopsitory;

namespace Movie_Console_App_Advance.Repository
{
    public class ReviewRepository : GenericRepository<Review>, IReviewRepository
    {
        public ReviewRepository(AppDbContext context) : base(context) { }
    }
}
