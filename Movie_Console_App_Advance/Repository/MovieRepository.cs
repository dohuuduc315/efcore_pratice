﻿using Microsoft.EntityFrameworkCore;
using Movie_Console_App_Advance.IRepopsitory;

namespace Movie_Console_App_Advance.Repository
{
    public class MovieRepository : GenericRepository<Movie>, IMovieRepository
    {
        public MovieRepository(AppDbContext context) : base(context) { }

        public async Task<Movie> GetMovieReview(int id) => await _dbSet.Include(x => x.Reviews).FirstOrDefaultAsync(x => x.Id == id);
    }
}
