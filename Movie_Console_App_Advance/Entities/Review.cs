﻿using Movie_Console_App_Advance.Entities;

namespace Movie_Console_App_Advance
{
    public class Review : BaseEntity
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public decimal Rating { get; set; }
        public int MovieId { get; set; }
        public Movie Movie { get; set; }
    }
}
