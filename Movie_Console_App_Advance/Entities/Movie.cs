﻿using Movie_Console_App_Advance.Entities;

namespace Movie_Console_App_Advance
{
    public class Movie : BaseEntity
    {
        public string Title { get; set; }
        public DateTime ReleaseDate { get; set; }
        public string Director { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public decimal Rating { get; set; }
        public ICollection<Review> Reviews { get; set; }
    }
}
