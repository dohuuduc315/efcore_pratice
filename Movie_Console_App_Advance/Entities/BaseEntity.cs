﻿namespace Movie_Console_App_Advance.Entities
{
    public class BaseEntity
    {
        public int Id { get; set; }
        public bool isDeleted { get; set; }
    }
}
