﻿namespace Movie_Console_App_Advance.IRepopsitory
{
    public interface IMovieRepository : IGenericRepository<Movie>
    {
        Task<Movie> GetMovieReview(int id);
    }
}
