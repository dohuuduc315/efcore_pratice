﻿using Movie_Console_App_Advance.Entities;

namespace Movie_Console_App_Advance.IRepopsitory
{
    public interface IGenericRepository<T> where T : BaseEntity
    {
        Task AddAsync(T entity);
        void Update(T entity);
        void SoftRemove(T entity);
        Task<List<T>> GetAllAsync(int pageIndex = 0, int pageSize = 10);
        Task<T?> GetByIdAsync(int id);
    }
}
