﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace Movie_Console_App_Advance.FluentAPIs
{
    class MovieConfiguration : IEntityTypeConfiguration<Movie>
    {
        public void Configure(EntityTypeBuilder<Movie> builder)
        {
            //congig movie with review
            builder.HasMany(x => x.Reviews)
                   .WithOne(x => x.Movie)
                   .HasForeignKey(x => x.MovieId);
        }
    }
}
