﻿using Movie_Console_App_Advance.IService;
using System.IO;

namespace Movie_Console_App_Advance.Service
{
    public class MovieService : IMovieService
    {
        private readonly IUnitOfWork _unitOfWork;

        public MovieService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public async Task AddMovie()
        {
            Console.Write("Enter Title: ");
            var title = Console.ReadLine();
            Console.Write("Enter Release Date: ");
            if (!DateTime.TryParse(Console.ReadLine(), out DateTime date))
            {
                Console.WriteLine("Invalid date");
                return;
            }
            Console.Write("Enter Director: ");
            var director = Console.ReadLine();
            Console.Write("Enter Description: ");
            var description = Console.ReadLine();
            Console.Write("Enter Image Url: ");
            var imageUrl = Console.ReadLine();
            Console.Write("Enter Rating: ");
            if (!decimal.TryParse(Console.ReadLine(), out decimal rating))
            {
                Console.WriteLine("Invalid rating");
                return;
            }
            var movie = new Movie
            {
                Title = title,
                ReleaseDate = date,
                Director = director,
                Description = description,
                ImageUrl = imageUrl,
                Rating = rating
            };
            await _unitOfWork.MovieRepository.AddAsync(movie);
            await _unitOfWork.SaveChangeAsync();
            Console.WriteLine("Add new movie successfully");
        }

        public async Task DeleteMovie(int id)
        {
            var movieObj = await _unitOfWork.MovieRepository.GetByIdAsync(id);
            if (movieObj is not null)
            {
                _unitOfWork.MovieRepository.SoftRemove(movieObj);
                await _unitOfWork.SaveChangeAsync();
                Console.WriteLine("Delete movie successfully");
            }
        }

        public async Task UpdateMovie(int id)
        {
            var movieObj = await _unitOfWork.MovieRepository.GetByIdAsync(id);
            if (movieObj is not null)
            {
                Console.Write("Enter Title: ");
                var title = Console.ReadLine();
                Console.Write("Enter Release Date: ");
                if (!DateTime.TryParse(Console.ReadLine(), out DateTime date))
                {
                    Console.WriteLine("Invalid date");
                    return;
                }
                Console.Write("Enter Director: ");
                var director = Console.ReadLine();
                Console.Write("Enter Description: ");
                var description = Console.ReadLine();
                Console.Write("Enter Image Url: ");
                var imageUrl = Console.ReadLine();
                Console.Write("Enter Rating: ");
                if (!decimal.TryParse(Console.ReadLine(), out decimal rating))
                {
                    Console.WriteLine("Invalid rating");
                    return;
                }
                movieObj.Title = string.IsNullOrEmpty(title) ? movieObj.Title : title;
                movieObj.Director = string.IsNullOrEmpty(director) ? movieObj.Director : director;
                movieObj.Description = string.IsNullOrEmpty(description) ? movieObj.Description : description;
                movieObj.ImageUrl = string.IsNullOrEmpty(imageUrl) ? movieObj.ImageUrl : imageUrl;
                movieObj.Rating = rating;

                _unitOfWork.MovieRepository.Update(movieObj);
                await _unitOfWork.SaveChangeAsync();
                Console.WriteLine("Update movie successfully");
            }
        }

        public async Task<List<Movie>> ViewAllMovie() => await _unitOfWork.MovieRepository.GetAllAsync();

        public async Task ViewMovieReview(int id)
        {
            var movieObj = await _unitOfWork.MovieRepository.GetByIdAsync(id);
            if (movieObj is null)
            {
                Console.WriteLine("Invalid movieId.");
                return;
            }

            Console.WriteLine($"{movieObj.Title} ({movieObj.ReleaseDate:d}) directed by {movieObj.Director}");

            if (movieObj.Reviews.Any())
            {
                foreach (var review in movieObj.Reviews)
                {
                    Console.WriteLine($"{review.Id}\t{review.Title}\t{review.Content}\t{review.Rating}");
                }
            }
            else
            {
                Console.WriteLine("No reviews found.");
            }
        }
    }
}
