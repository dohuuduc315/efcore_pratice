﻿using Movie_Console_App_Advance.IService;

namespace Movie_Console_App_Advance.Service
{
    public class ReviewService : IReviewService
    {
        private readonly IUnitOfWork _unitOfWork;

        public ReviewService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public async Task AddReview()
        {
            Console.Write("Enter Movie Id: ");
            if (!int.TryParse(Console.ReadLine(), out int movieId))
            {
                Console.WriteLine("Invalid Id");
                return;
            }
            Console.Write("Enter Title: ");
            var title = Console.ReadLine();
            Console.Write("Enter Content: ");
            var content = Console.ReadLine();
            Console.Write("Enter Rating: ");
            if (!decimal.TryParse(Console.ReadLine(), out decimal rating))
            {
                Console.WriteLine("Invalid rating");
                return;
            }
            var review = new Review
            {
                MovieId = movieId,
                Title = title,
                Content = content,
                Rating = rating
            };
            await _unitOfWork.ReviewRepository.AddAsync(review);
            await _unitOfWork.SaveChangeAsync();
            Console.WriteLine("Add new review successfully");
        }

        public async Task DeleteReview(int id)
        {
            var reviewObj = await _unitOfWork.ReviewRepository.GetByIdAsync(id);
            if (reviewObj is not null)
            {
                _unitOfWork.ReviewRepository.SoftRemove(reviewObj);
                await _unitOfWork.SaveChangeAsync();
                Console.WriteLine("Delete review successfully");
            }
        }

        public async Task UpdateReview(int id)
        {
            var reviewObj = await _unitOfWork.ReviewRepository.GetByIdAsync(id);
            if (reviewObj is not null)
            {
                Console.Write("Enter Title: ");
                var title = Console.ReadLine();
                Console.Write("Enter Content: ");
                var content = Console.ReadLine();
                Console.Write("Enter Rating: ");
                if (!decimal.TryParse(Console.ReadLine(), out decimal rating))
                {
                    Console.WriteLine("Invalid rating");
                    return;
                }
                reviewObj.Title = string.IsNullOrEmpty(title) ? reviewObj.Title : title;
                reviewObj.Content = string.IsNullOrEmpty(content) ? reviewObj.Content : content;
                reviewObj.Rating = rating;
                _unitOfWork.ReviewRepository.Update(reviewObj);
                await _unitOfWork.SaveChangeAsync();
                Console.WriteLine("Update review successfully");
            }
        }
    }
}
